package com.experiment.listsample_java;

public class UserData {

    private String userName, userNumber;

    public UserData(final String userName, final String userNumber) {
        this.userName = userName;
        this.userNumber = userNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(final String userNumber) {
        this.userNumber = userNumber;
    }
}
