package com.experiment.listsample_java;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SampleListAdapter extends RecyclerView.Adapter<UserDataViewHolder> {

    private Context context;
    private List<UserData> userData;

    public SampleListAdapter(final Context context, final List userData) {
        this.context = context;
        this.userData = userData;
    }

    @Override
    public int getItemCount() {
        return userData.size();
    }

    @NonNull
    @Override
    public UserDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View itemView = layoutInflater.inflate(R.layout.item_user_data, parent, false);
        return new UserDataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserDataViewHolder holder, int position) {
        holder.bind(userData.get(position));
    }
}
