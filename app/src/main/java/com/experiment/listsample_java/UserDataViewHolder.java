package com.experiment.listsample_java;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UserDataViewHolder extends RecyclerView.ViewHolder {

    private View itemView;

    public UserDataViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    void bind(UserData userData) {
        final TextView nameView = itemView.findViewById(R.id.name);
        final TextView phoneNumberView = itemView.findViewById(R.id.phone_number);

        nameView.setText(userData.getUserName());
        phoneNumberView.setText(userData.getUserNumber());
    }
}
