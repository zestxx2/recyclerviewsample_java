package com.experiment.listsample_java;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Получаем экземпляр RecyclerView по id
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        //Создаем adapter
        SampleListAdapter sampleListAdapter = new SampleListAdapter(this, getFakeDataList());
        //Создаем простой LayoutManager
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
        //Передаем значения Recycler
        recyclerView.setAdapter(sampleListAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    List<UserData> getFakeDataList() {
        final List<UserData> userDataList = new ArrayList<UserData>();
        userDataList.add(new UserData("Иван", "+79559944998"));
        userDataList.add(new UserData("Саша", "+79559244998"));
        userDataList.add(new UserData("Инга", "+79552344998"));
        userDataList.add(new UserData("Оля", "+79555674998"));
        userDataList.add(new UserData("Рома", "+79559454998"));
        userDataList.add(new UserData("Тема", "+79554644998"));
        userDataList.add(new UserData("Иван", "+79555564998"));

        return userDataList;
    }
}
